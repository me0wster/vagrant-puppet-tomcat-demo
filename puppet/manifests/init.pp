Exec { path => [ "/bin/", "/sbin/" , "/usr/bin/", "/usr/sbin/" ] }

class { 'jdk_oracle': }

include tomcat
tomcat::install { '/opt/tomcat':
  source_url => 'http://www-us.apache.org/dist/tomcat/tomcat-7/v7.0.70/bin/apache-tomcat-7.0.70.tar.gz',
}
tomcat::instance { 'default':
  catalina_home => '/opt/tomcat',
}
tomcat::war { 'sample.war':
  catalina_base => '/opt/tomcat',
  war_source    => 'https://tomcat.apache.org/tomcat-7.0-doc/appdev/sample/sample.war',
}

class { 'apache':  }
