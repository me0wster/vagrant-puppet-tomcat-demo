#!/usr/bin/env bash
set -e
# Directory in which PuppetFile is placed to be scanned by librarian-puppet.
PUPPET_DIR=/vagrant/puppet

echo "Installing Git.."
apt-get -q -y install git

echo "Installing librarian-puppet.."
if [ "$(gem search -i librarian-puppet)" = "false" ]; then
  gem install librarian-puppet
fi
echo "librarian-puppet installed!"
echo "Executing PuppetFile.."
cd $PUPPET_DIR && librarian-puppet install --path modules

echo "Installing Puppet repo for Ubuntu 14.04 LTS"
cd /tmp
wget https://apt.puppetlabs.com/puppetlabs-release-trusty.deb
dpkg -i puppetlabs-release-trusty.deb
apt-get update
echo Installing puppet
apt-get install -y puppet
echo "Puppet installed!"

#puppet module install puppetlabs-ruby
#puppet module install puppetlabs-stdlib
#puppet module install evenup-tomcat
